#Classes 

oo::class create Animal {
	variable nom
	variable age 
	variable espece
	#constructor {n} { set nom $n}
	constructor {{n ""} {a 0} {e ""}} { 
	set nom $n
	set age $a
	set espece $e
	}
	destructor { puts "$nom detruit"}

	method setNom {n} { set nom $n }
	method setAgeEnMois {m} { my setAge [expr $m/12]}
	method setAge {n} { set age $n }
	method setEspece {n} { set espece $n }
	method afficher {} { puts "$nom $age $espece"}
}

oo::class create Encadreur {
	method encadrer {} {
		puts "************"
		my afficher
		puts "*************"
		}
}

oo::class create Reptile {
	superclass Animal
	mixin Encadreur
	variable amphibien
	constructor {{n ""} {a 0} {e ""} {am ""}} { 
		next $n $a $e
		set amphibien $am
	}
	method setAmphibien {am} { set amphibien $am}
	method afficher {} { 
		next
		puts "$amphibien"}

}

set ann [Animal new]
$ann setNom "Ann"
$ann setAge 23
$ann setEspece Girafe
$ann afficher
#puts $ann 

set tom [Animal new "Tom" 19 chat]
$tom afficher
$tom destroy

set frank [Reptile new "Phil" 3 Caiman 1]
#$frank afficher	
$frank encadrer	