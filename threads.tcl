# threads
package require Thread

tsv::set lapins resultat 0
tsv::set lapins2 time 0

#puts "Au bout de 6 mois :[nblapins 6]"
set threadcalcul [thread::create -joinable {
	proc nblapins {mois} {

		if {$mois<3 || [tsv::get lapins2 time]} {
			return $mois
		} else { return [expr 3*([nblapins [expr $mois-1]]+ \
			[nblapins [expr $mois-2]])/2 ]
		}

	}
	tsv::lock lapins {
		tsv::set lapins resultat [nblapins 32]
	}
	# puts "[tsv::get lapins time]"
	# if {[tsv::get lapins time]<10} {

	# }
}]
puts "Calcul"
puts "Au bout de 2 ans et 6 mois :"
#for {set i 0} {$i<10} {incr i} {
for {set i 0} {[thread::exists $threadcalcul]} {incr i} {
	puts -nonewline "."
	flush stdout
	after 100

	if {$i==3} {tsv::set lapins2 time 1}
}
tsv::lock lapins {
	puts "[tsv::get lapins resultat]"
}
#thread::join $threadcalcul