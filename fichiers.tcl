# fichiers

if {[catch { [ expr 3/0] } err ]} {
	puts "Error : $err"
}


if {[catch { error "Plus de cacahuettes" } err ]} {
	puts "Error : $err"
}

if {[catch { error "Plus de cacahuettes" "Enclos des singes" 10 } err ]} {
	puts "Error : $err ! $errorInfo ($errorCode)"
}





if {[catch { 
	if {![file exists data]} {
		file mkdir data
	}
	cd data
	puts "Repertoir en cours : [pwd] "
	foreach fichier [glob -nocomplain *.*] {
		puts "- $fichier"
	}
	# log210408.txt
	set nomfichier "log : [clock format [clock seconds] -format %Y-%m-%d].txt"
	puts "Remplissage de $nomfichier"
	set f1 [open $nomfichier a]
	
	puts $f1 "log : [clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}].txt"
	close $f1
	
	set f2 [open $nomfichier r]
	
	set nblignes 0
	while { [gets $f2 ligne] >= 0 } {
		incr nblignes
	}
		
	close $f2		
	puts "$nblignes de log dans le fichier"
	
	
} err ]} {
	puts "Error : $err ! $errorInfo ($errorCode)"
}