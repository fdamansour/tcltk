# declaration de package
package require Tcl 8.5
package provide Voliere 1.2

namespace eval Voliere {
	variable especes [list Aigles Vautours Milans Peroquets]
	namespace export afficher
	namespace export ajouter
}

proc Voliere::afficher {} {
	variable especes
	puts "Notre voliere accueille :"
	foreach espece $especes {
		puts "- des [string tolower $espece]"
	}
}

proc Voliere::ajouter {espece} {
	variable especes
	lappend especes $espece
}


#Voliere::afficher
