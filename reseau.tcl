# Reseau - sockets
package require Thread

set threadserveur [thread::create -joinable {
	set stop 0
	proc on_message {connexion} {
		gets $connexion nom
		if {$nom eq "STOP"} {
			global stop
			set stop 1
		} else {
			switch $nom {
				Ann {set age 22}
				Bob {set age 8}
				Franck {set age 14}	
				default {set age 0}
			}
			puts $connexion "$nom : $age"
			flush $connexion 
		}
	}
	proc on_connect {connexion adresse port} {
		puts "Client connecte $adresse:$port"
		puts $connexion "Il est [clock format \
			[clock seconds] -format {%Y-%m-%d %H:%M:%S}] au Zoo"
		flush $connexion
		fileevent $connexion readable "on_message $connexion"
	}
	socket -server on_connect 11000
	puts "Serveur demarre"
	vwait stop
	puts "Serveur arrete"
}]

after 1000
set client [socket localhost 11000]
gets $client reponse
puts "Reponse du serveur : $reponse"

puts $client "Bob"
flush $client
gets $client reponse
puts "Reponse du serveur : $reponse"

after 1000

#close $client
puts $client STOP
flush $client

thread::join $threadserveur
