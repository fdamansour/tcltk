# BD

package require sqlite3

if {[catch {
	cd data
	sqlite3 db zoo.sqlite
	db eval {CREATE TABLE IF NOT EXISTS animaux (id INTEGER PRIMARY KEY, 
		nom TEXT, espece TEXT)}
	set animaux [list 
		[list Bob Tigre] \
		[list Ann Girafe] \
		[list Helene Crocodile]
	]
	foreach animal animaux{
		set nom [lindex $animal 0]
		set espece [lindex $animal 1]
		puts "Insertion de $nom"
		db eval {INSERT INTO animaux(nom,espece)VALUES($nom,$espece)}
	}
	
	db eval { SELECT COUNT (*) AS c FROM animaux} {
		puts "il y a $c animaux."
	}
	db eval {SELECT * FROM animaux}{
		puts "$id $nom $espece"
	}
	
	db close
} err ]} {
	puts "Error : $err ! $errorInfo ($errorCode)"
}