# Fenetres TK

package require Tk

proc test_fenetres {} {
tk::toplevel .fenetre2
tk::toplevel .fenetre3
wm title .fenetre2 "Fenetre numéro 2"
# 800px de large, 500 de haut, à 100 de la gauche, à 0 du haut
wm geometry .fenetre3 800x500+100+0
wm attributes .fenetre3 -alpha 0.5 -topmost 1

}

wm title . "Alerte du zoo" 
wm geometry . 600x600

tk::frame .panneau1 -width 580 -height 280 -relief raised -bd 2
place .panneau1 -x 10 -y 10
tk::labelframe .panneau2 -width 580 -height 280 -text Informationssss
place .panneau2 -x 10 -y 310

tk::label .panneau1.label1 -text {Titre :}
place .panneau1.label1 -x 10 -y 10

tk::entry .panneau1.entry1 
place .panneau1.entry1  -x 10 -y 50

tk::label .panneau1.label2 -text {Lieu :}
place .panneau1.label2 -x 10 -y 90

tk::entry .panneau1.entry2 
place .panneau1.entry2  -x 10 -y 130

tk::checkbutton .panneau1.checkbutton -text {Lieu :}
place .panneau1.checkbutton -x 10 -y 170

tk::button .panneau1.button -text {Lieu :}
place .panneau1.button -x 10 -y 210

tk::label .panneau2.informations -text {Resultat :}
place .panneau2.informations -x 10 -y 10