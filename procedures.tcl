# procedures du zoo
proc ditbonjour {} { puts "Hello Zoo !" }

proc ditbonjouralanimal {{nom ""} {excl !}} { 
	set text  "Hello $nom $excl" 
	puts $text
}

# Interdit puts $text

proc ditbonjouranimaux args {
	foreach animal $args {
		ditbonjouralanimal $animal
	}
}

proc getcri {animal} {
	proc getcrifauve {} {return rugit }
	switch $animal {
		tigre { return [getcrifauve] }
		girafe { return meugle }
		faucon { return huit }
		default {return cri }
	}
}
# proc incr {novariable} { set $novariable [expr $$nomvariable+1] }
# set i 18
# incr i

proc tarifpour {visiteurs} {
	#global tarif
	#return [expr $tarif*$visiteurs]

	upvar tarif tarifloc
	return [expr $tarifloc*$visiteurs]
}
proc decr {nomvariable} {
	upvar $nomvariable nomvariableloc

	set nomvariableloc [expr $nomvariableloc-1]
}
proc afficheinfos {} {
	set tarif 10.3
	decr tarif
	puts "WE special pour 4 : [tarifpour 4]"
	puts $tarif
}
ditbonjour
ditbonjouralanimal
ditbonjouralanimal "Ann la girafe"
ditbonjouralanimal "Tom le tigre" !!!
ditbonjouranimaux Ann Tom Carol
puts "La girafe [getcri girafe]"
puts "Le tigre [getcri tigre]"
set tarif 8.4
puts "Tarif pour 2 : [tarifpour 2]"
afficheinfos